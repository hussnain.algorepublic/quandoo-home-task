import 'dart:async';

import 'package:flutter/material.dart';
import 'package:quandoo_home_task/helper/navigation_helper.dart';
import 'package:quandoo_home_task/helper/api_manager.dart';
import 'package:quandoo_home_task/widgets/app_error_widget.dart';
import 'locator.dart';
void main() {
  setUpGetItLocator();
  ErrorWidget.builder = (errorDetails) {
    return AppErrorWidget(
      errorDetails: errorDetails,
    );
  };
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: Splash(title: 'Flutter Demo Home Page'),
    );
  }
}

class Splash extends StatefulWidget {
  Splash({Key key,  this.title}) : super(key: key);

  final String title;

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<Splash> {


  @override
  void initState() {
    super.initState();
    startTime();
  }

  void startTime() async {
    Timer(Duration(seconds: 1), navigateToMerchantListView);
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(

      body: Center(
        child: Text(
              'Quandoo',
              style: Theme.of(context).textTheme.headline4,
            ),

      ),
    );
  }
  void navigateToMerchantListView(){
    NavigationManager.navigateToMerchantListView(context);
  }


}
