class MerchantImages {
  MerchantImages({
    this.url,
  });

  String url;

  factory MerchantImages.fromJson(Map<String, dynamic> json) => MerchantImages(
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "url": url,
  };

  String getUrl(){
    return url??"";
  }
}
