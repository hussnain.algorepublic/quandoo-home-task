// To parse this JSON data, do
//
//     final merchantEntityModel = merchantEntityModelFromJson(jsonString);

import 'dart:convert';

import 'package:quandoo_home_task/model/merchant.dart';

MerchantEntityModel merchantEntityModelFromJson(String str) => MerchantEntityModel.fromJson(json.decode(str));

String merchantEntityModelToJson(MerchantEntityModel data) => json.encode(data.toJson());

class MerchantEntityModel {
  MerchantEntityModel({
     this.merchants,
     this.size,
     this.offset,
     this.limit,
  });

  List<Merchant> merchants;
  int size;
  int offset;
  int limit;

  factory MerchantEntityModel.fromJson(Map<String, dynamic> json) => MerchantEntityModel(
    merchants: List<Merchant>.from(json["merchants"].map((x) => Merchant.fromJson(x))),
    size: json["size"]??0,
    offset: json["offset"]??0,
    limit: json["limit"]??0,
  );

  Map<String, dynamic> toJson() => {
    "merchants": List<dynamic>.from(merchants.map((x) => x.toJson())),
    "size": size,
    "offset": offset,
    "limit": limit
  };

  List getMerchant(){
   return merchants??[];
  }

  int getLimit(){
    return limit??0;
  }

  int getOffSet(){
    return offset??0;
  }
}











