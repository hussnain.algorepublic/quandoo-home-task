class StandardOpeningTimes {
  StandardOpeningTimes({
    this.wednesday,
    this.monday,
    this.thursday,
    this.sunday,
    this.friday,
    this.saturday,
    this.tuesday,
  });

  List<Day> wednesday;
  List<Day> monday;
  List<Day> thursday;
  List<Day> sunday;
  List<Day> friday;
  List<Day> saturday;
  List<Day> tuesday;

  factory StandardOpeningTimes.fromJson(Map<String, dynamic> json) => StandardOpeningTimes(
    wednesday: List<Day>.from(json["WEDNESDAY"].map((x) => Day.fromJson(x))),
    monday: List<Day>.from(json["MONDAY"].map((x) => Day.fromJson(x))),
    thursday: List<Day>.from(json["THURSDAY"].map((x) => Day.fromJson(x))),
    sunday: List<Day>.from(json["SUNDAY"].map((x) => Day.fromJson(x))),
    friday: List<Day>.from(json["FRIDAY"].map((x) => Day.fromJson(x))),
    saturday: List<Day>.from(json["SATURDAY"].map((x) => Day.fromJson(x))),
    tuesday: List<Day>.from(json["TUESDAY"].map((x) => Day.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "WEDNESDAY": List<dynamic>.from(wednesday.map((x) => x.toJson())),
    "MONDAY":    List<dynamic>.from(monday.map((x) => x.toJson())),
    "THURSDAY": List<dynamic>.from(thursday.map((x) => x.toJson())),
    "SUNDAY": List<dynamic>.from(sunday.map((x) => x.toJson())),
    "FRIDAY": List<dynamic>.from(friday.map((x) => x.toJson())),
    "SATURDAY": List<dynamic>.from(saturday.map((x) => x.toJson())),
    "TUESDAY":  List<dynamic>.from(tuesday.map((x) => x.toJson())),
  };
}


class Day {
  Day({
    this.start,
    this.end,
  });

  String start;
  String end;

  factory Day.fromJson(Map<String, dynamic> json) => Day(
    start: json["start"],
    end: json["end"],
  );

  Map<String, dynamic> toJson() => {
    "start": start,
    "end": end,
  };
}