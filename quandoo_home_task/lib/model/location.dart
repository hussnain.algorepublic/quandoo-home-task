
import 'package:quandoo_home_task/model/address.dart';
import 'package:quandoo_home_task/model/coordinate.dart';

class Location {
  Location({
    this.coordinates,
    this.address,
  });

  Coordinates coordinates;
  Address address;

  factory Location.fromJson(Map<String, dynamic> json) => Location(
    coordinates: Coordinates.fromJson(json["coordinates"]),
    address: Address.fromJson(json["address"]),
  );

  Map<String, dynamic> toJson() => {
    "address": address.toJson(),
    "coordinates": coordinates.toJson(),
  };

  Coordinates getCoordinate(){
    return coordinates??Coordinates();
  }

  Address getAddress(){
    return address??Address();
  }
}
