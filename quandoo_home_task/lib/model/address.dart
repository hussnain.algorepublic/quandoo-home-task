class Address {
  Address({
    this.street,
    this.number,
    this.zipcode,
    this.city,
    this.country,
    this.district,
  });

  String street;
  String number;
  String zipcode;
  String city;
  String country;
  String district;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    street: json["street"],
    number: json["number"],
    zipcode: json["zipcode"],
    city: json["city"],
    country: json["country"],
    district: json["district"],
  );

  Map<String, dynamic> toJson() => {
    "street": street,
    "number": number,
    "zipcode": zipcode,
    "city": city,
    "country": country,
    "district": district,
  };

  String getStreet(){
    return street??"";
  }
  String getNumber(){
    return number??"";
  }
  String getZipCode(){
    return zipcode??"";
  }
  String getCity(){
    return city??"";
  }
  String getCountry(){
    return country??"";
  }
  String getDistrict(){
    return district??"";
  }


}
