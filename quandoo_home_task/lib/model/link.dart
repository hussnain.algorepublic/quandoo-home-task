class Link {
  Link({
    this.href,
    this.method,
    this.rel,
  });

  String href;
  String method;
  String rel;

  factory Link.fromJson(Map<String, dynamic> json) => Link(
    href: json["href"],
    method: json["method"],
    rel: json["rel"],
  );

  Map<String, dynamic> toJson() => {
    "href": href,
    "method": method,
    "rel": rel,
  };

  String getHref(){
    return href??"";
  }

  String getMethod(){
    return method??"";
  }

  String getRel(){
    return rel??"";
  }
}
