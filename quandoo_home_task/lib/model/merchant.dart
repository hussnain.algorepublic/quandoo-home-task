


import 'package:quandoo_home_task/model/link.dart';
import 'package:quandoo_home_task/model/location.dart';
import 'package:quandoo_home_task/model/merchant_image.dart';

class Merchant {
  Merchant({
    this.id,
    this.name,
    this.phoneNumber,
    this.currency,
    this.locale,
    this.timezone,
    this.location,
    this.reviewScore,
    this.images,
    this.links,
    this.bookable,
    this.ccvEnabled,
  });

  int id;
  String name;
  String phoneNumber;
  String currency;
  String locale;
  String timezone;
  Location location;
  String reviewScore;
  List<MerchantImages> images;
  List<Link> links;
  bool bookable;
  bool ccvEnabled;

  factory Merchant.fromJson(Map<String, dynamic> json) => Merchant(
    id: json["id"]??'',
    name: json["name"]??"N/A",
    phoneNumber: json["phoneNumber"]??"N/A",
    currency: json["currency"]??"N/A",
    locale: json["locale"]??"N/A",
    timezone: json["timezone"]??"N/A",
    location: Location.fromJson(json["location"]),
    reviewScore: json["reviewScore"]??"N/A",
    images: List<MerchantImages>.from(json["images"].map((x) => MerchantImages.fromJson(x))),
    links: List<Link>.from(json["links"].map((x) => Link.fromJson(x))),
    bookable: json["bookable"],
    ccvEnabled: json["ccvEnabled"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "phoneNumber": phoneNumber,
    "currency": currency,
    "locale": locale,
    "timezone": timezone,
    "location": location.toJson(),
    "reviewScore": reviewScore,
    "images": List<dynamic>.from(images.map((x) => x.toJson())),
    "links": List<dynamic>.from(links.map((x) => x.toJson())),
    "bookable": bookable,
    "ccvEnabled": ccvEnabled,
  };

  String getReviewScore(){
    return reviewScore??"";
  }

  String getName(){
    return name??"";
  }

  String getNumber(){
    return phoneNumber??"";
  }

  List<Link> getLinks(){
    return links??[];
  }

  List<MerchantImages> getImages(){
    return images??[];
  }

  Location getLocation(){
    return location??Location();
  }





}
