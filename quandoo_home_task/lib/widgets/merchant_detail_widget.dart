
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:quandoo_home_task/helper/common.dart';
import 'package:quandoo_home_task/helper/text_styles.dart';
import 'package:quandoo_home_task/helper/global.dart' as globals;
import 'package:carousel_slider/carousel_slider.dart';
import 'package:quandoo_home_task/merchant/merchant_controller.dart';
import 'package:quandoo_home_task/model/merchant.dart';

class MerchantDetailWidget extends StatefulWidget {
  final Merchant merchant;
  final MerchantController merchantController;

  const MerchantDetailWidget({Key key, this.merchant,this.merchantController})
      : super(key: key);

  @override
  _MerchantDetailWidgetState createState() =>
      _MerchantDetailWidgetState(this.merchant,this.merchantController);
}

class _MerchantDetailWidgetState extends State<MerchantDetailWidget> {
  final Merchant merchant;
  final MerchantController merchantController;
  _MerchantDetailWidgetState(this.merchant,this.merchantController);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Column(
        children: [
          ///Slider
          imageSlider(),

          ///Name
          nameWidget(),

          /// Phone number widget
          phoneNumberWidget(),

          /// address widget
          addressWidget(),

          /// website widget
          websiteWidget(),
        ],
      ),
    );
  }


  /// get complete address
  String getAddress() {
    var address=merchant.getLocation().getAddress();
    return "${address.street} ${address.number}, ${address.zipcode}${address.city}";
  }

  /// image slider widget
  Widget imageSlider() {
    return CarouselSlider(
      options: CarouselOptions(
        height: 200,
        autoPlay: true,
        pageSnapping: true,
        autoPlayInterval: const Duration(seconds: 5),
      ),
      items: merchant.getImages().map((i) {
        return Builder(
          builder: (BuildContext context) {
            return Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.symmetric(horizontal: 5.0),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Image(
                  image: CachedNetworkImageProvider(i.url),
                  fit: BoxFit.cover,
                ));
          },
        );
      }).toList(),
    );
  }

  /// Name widget
  Widget nameWidget(){
    return Container(
      width: MediaQuery.of(context).size.width,

      child: Row(children: [
        Container(

          margin: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          child: Text(
            merchant.getName(),
            style: TextStyles.getNameStyle(globals.listTextColor),
            textAlign: TextAlign.left,
          )),
        Spacer(),
        Container(
          margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
          child: Column(children: [
          Container(
              height: 40,
              width: 40,

              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: globals.highReviewColor,
                  border: Border.all(

                  ),
                  borderRadius: BorderRadius.all(Radius.circular(200))
              ),

              child: Text(merchant.getReviewScore(),
                style: TextStyles.getReviewTextStyle(globals.backgroundColor),
              )
          ),
          Container(
              child: Text("Review",
              )
          )
        ],),)

      ],)



    );
  }

  /// Phone number widget
  Widget phoneNumberWidget(){
    return Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: InkWell(
          child: Row(
            children: [
              Icon(
                Icons.phone,
                color: globals.dividerColor,
              ),
              Expanded(
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(
                        vertical: 10, horizontal: 10),
                    child: Text(
                      merchant.getNumber(),
                      textAlign: TextAlign.left,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyles.getUnderLineTextStyle(globals.blackColor),
                    )),
              )
            ],
          ),
          onTap: () {
            merchantController.makeCall(merchant.getNumber());
          },
        ));
  }

  /// Address widget
  Widget addressWidget(){
    return  Container(
        margin: EdgeInsets.symmetric(horizontal: 10),
        child: InkWell(
          child: Row(
            children: [
              Icon(
                Icons.room_rounded,
                color: globals.dividerColor,
              ),
              Expanded(
                  child: Container(
                      width: MediaQuery.of(context).size.width,
                      margin: EdgeInsets.symmetric(
                          vertical: 10, horizontal: 10),
                      child: Text(
                        getAddress().toString(),
                        textAlign: TextAlign.left,
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyles.getUnderLineTextStyle(globals.blackColor),
                      )))
            ],
          ),
          onTap: () {
            merchantController.launchMapForAddress(
                getAddress().toString(),
                merchant.getLocation().getCoordinate().getLatitude(),
                merchant.getLocation().getCoordinate().getLongitude());
          },
        ));
  }

  /// Website widget
  Widget websiteWidget(){
    return Visibility(
        visible: merchant.getLinks().isNotEmpty?true:false,
        child: Container(
            margin: EdgeInsets.symmetric(horizontal: 10),
            child: InkWell(
              child: Row(
                children: [
                  Icon(
                    Icons.open_in_browser,
                    color: globals.dividerColor,
                  ),
                  Expanded(
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(
                              vertical: 10, horizontal: 10),
                          child: Text(
                            "Website",
                            textAlign: TextAlign.left,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyles.getUnderLineTextStyle(
                                globals.blackColor),
                          )))
                ],
              ),
              onTap: () {
                merchantController.launchWebUrl(merchant.getLinks().elementAt(0).getHref());
              },
            )));
  }
}
