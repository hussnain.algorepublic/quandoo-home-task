import 'package:flutter/material.dart';
import 'package:quandoo_home_task/helper/global.dart' as globals;

/// This class is responsible for handling crashes
/// and exceptions you can even send crash detail
/// to the server it is like Crashlytics
///

class AppErrorWidget extends StatelessWidget {
  final FlutterErrorDetails errorDetails;

  const AppErrorWidget({Key key, this.errorDetails}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            globals.errorTitle,
          ),
          centerTitle: true,
        ),
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          decoration: BoxDecoration(border: Border.all(color: Colors.red[200])),
          child: Center(
            child: Text(
              globals.errorMessage,
              style: TextStyle(
                fontSize: 25,
                color: Colors.red,
              ),
              textAlign: TextAlign.center,
            ),
          ),
        ),
      ),
    );
  }
}
