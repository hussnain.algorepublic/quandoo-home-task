import 'package:flutter/material.dart';
import 'package:mvc_application/controller.dart' show ControllerMVC;
import 'package:quandoo_home_task/helper/common.dart';
import 'package:quandoo_home_task/helper/api_manager.dart';
import 'package:quandoo_home_task/locator.dart';
import 'package:quandoo_home_task/model/merchant.dart';
import 'package:quandoo_home_task/model/merchant_image.dart';

class MerchantController extends ControllerMVC{
  factory MerchantController() => _this ??= MerchantController._();
  ApiManager repository=ApiManager();
  int index=0;
  bool isLoading=true;
  MerchantController._(){
    repository =locator<ApiManager>();
  }

  static MerchantController _this;

  /// set Index
  void setIndex(int index){
    this.index=index;
  }

  /// get index
  int get getIndex =>index??0;


  ///set loading
  void setLoading(bool value){
    this.isLoading=value;
  }

  /// get loading
  bool get getLoading => isLoading??false;


  /// fetching the merchant list
  Future getMerchantList(BuildContext context) async{
    await repository.getMerchantList(context,100);
  }

  /// get the first image for main list
  String getImageForList(int index){
    if(repository!=null){
      return repository.merchantEntityModel.merchants.elementAt(index).images.isNotEmpty?
      repository.merchantEntityModel.merchants.elementAt(index).images.elementAt(0).url:"";
    }
    else{
      return "";
    }
  }

  /// get the limit of list
  int getListLimit(){
    if(repository!=null){
      return repository.merchantEntityModel!=null?repository.merchantEntityModel.limit:0;
    }
    else{
      return 0;
    }
  }


  /// get the list of image for sliding
  List<MerchantImages> getImagesForSlider(int index){
    if(repository!=null){
      return repository.merchantEntityModel!=null?repository.merchantEntityModel.merchants.elementAt(index).images:[];
    }
    else{
      return [];
    }
  }

  ///  get selected merchant
  Merchant getMerchant(){
    return repository.merchantEntityModel.getMerchant().elementAt(index);
  }

  /// This function is use for making call
  void makeCall(String phoneNumber){
    Common.launchLink('tel:$phoneNumber');
  }

  /// This function is use for launch web Url
  void launchWebUrl(String webUrl){
    Common.launchLink(webUrl);
  }

  void launchMapForAddress(String address,double lat,long ){
    Common.launchMap(
        address,
        lat,
        long);
  }


}