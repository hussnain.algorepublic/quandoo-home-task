import 'package:flutter/material.dart';
import 'package:mvc_application/view.dart' show StateMVC;
import 'package:quandoo_home_task/helper/navigation_helper.dart';
import 'package:quandoo_home_task/helper/text_styles.dart';
import 'package:quandoo_home_task/merchant/merchant_controller.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:quandoo_home_task/helper/global.dart' as globals;
import 'package:quandoo_home_task/widgets/merchant_detail_widget.dart';

class MerchantListView extends StatefulWidget {
  @override
  _MerchantListViewState createState() => _MerchantListViewState();
}

class _MerchantListViewState extends StateMVC<MerchantListView> {
  MerchantController merchantController;
  var orientation;
  MerchantDetailWidget merchantDetailWidget;

  _MerchantListViewState() : super(MerchantController()) {
    merchantController = controller as MerchantController;
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    setOrientation(context);

    return Scaffold(
      backgroundColor: globals.backgroundColor,
      appBar: AppBar(
        title: Text("Merchants"),
        centerTitle: true,
      ),
      body: !merchantController.getLoading?
          /// main list widget
          Container(
              child: Row(
              children: [
                /// merchant list
                Expanded(
                    flex: orientation == Orientation.landscape ? 3 : 10,
                    child: merchantListWidget()),

                /// divider between list and detail
                Visibility(
                    visible:
                        orientation == Orientation.landscape ? true : false,
                    child: Container(
                      height: MediaQuery.of(context).size.height,
                      width: 1,
                      color: globals.dividerColor,
                    )),

                /// merchant detail
                Visibility(
                    visible:
                        orientation == Orientation.landscape ? true : false,
                    child: Expanded(
                        flex: 7,
                        child: Container(
                            child: SingleChildScrollView(
                                child: InkWell(
                          child: MerchantDetailWidget(
                            merchant: merchantController.getMerchant(),
                            merchantController: merchantController,
                          ),
                          onTap: () {
                            NavigationManager.navigateToMerchantListDetailView(
                                context, merchantController.getMerchant(),merchantController);
                          },
                        ))))),
              ],
            ))
          :

          /// loading widget
          Container(
              alignment: Alignment.center,
              color: Colors.transparent,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Center(child: CircularProgressIndicator())),
            ),
    );
  }

  ///Merchant list view Widget
  Widget merchantListWidget() {
    return Container(
        color: Colors.white,
        child: ListView.builder(
          primary: false,
          padding: EdgeInsets.only(left: 5.0),
          shrinkWrap: true,
          itemCount: merchantController.getListLimit(),
          itemBuilder: (context, index) {
            return Container(
              margin: EdgeInsets.symmetric(
                vertical: 5,
              ),
              child: Column(
                children: <Widget>[
                  Row(
                    children: [
                      Container(
                        height: 50,
                        width: 50,
                        margin: EdgeInsets.symmetric(
                          vertical: 10,
                        ),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(200.0),
                          child: CachedNetworkImage(
                            imageUrl: merchantController.getImageForList(index),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Expanded(
                        child: ListTile(
                          title: Text(
                              merchantController
                                  .repository.merchantEntityModel.merchants
                                  .elementAt(index)
                                  .getName(),
                              textAlign: TextAlign.left,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyles.getListTextStyle(
                                  globals.listTextColor)),
                          onTap: () async {
                            ///set layout list with detail
                            merchantController.setIndex(index);
                            if (orientation != Orientation.landscape) {
                              NavigationManager
                                  .navigateToMerchantListDetailView(context,
                                      merchantController.getMerchant(),merchantController);
                            } else {
                              Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          this.build(context)));
                            }
                          },
                        ),
                      )
                    ],
                  ),
                  Divider(
                    height: 1,
                    color: globals.dividerColor,
                  )
                ],
              ),
            );
          },
        ));
  }

  /// get List by calling an api from controller
  void getData() async {
    if (merchantController.repository.merchantEntityModel == null) {
      await merchantController.getMerchantList(context);
      if (merchantController.repository.merchantEntityModel != null) {
        merchantController.setLoading(false);
      }
      if (mounted) {
        setState(() {});
      }
    }
  }

  ///set an Orientation
  void setOrientation(BuildContext context) {
    if (orientation == null) {
      orientation = MediaQuery.of(context).orientation;
      if (mounted) {
        setState(() {
          print(orientation);
        });
      }
    } else {
      if (orientation != MediaQuery.of(context).orientation) {
        orientation = MediaQuery.of(context).orientation;
        if (mounted) {
          setState(() {
            print(orientation.toString());
          });
        }
      }
    }
  }
}
