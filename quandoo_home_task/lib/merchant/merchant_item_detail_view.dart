
import 'package:flutter/material.dart';
import 'package:quandoo_home_task/merchant/merchant_controller.dart';
import 'package:quandoo_home_task/helper/global.dart' as globals;
import 'package:quandoo_home_task/model/merchant.dart';
import 'package:quandoo_home_task/widgets/merchant_detail_widget.dart';

class MerchantListViewDetail extends StatefulWidget {
  final Merchant merchant;
  final MerchantController merchantController;

  const MerchantListViewDetail({Key key, this.merchant,this.merchantController}) : super(key: key);

  @override
  _MerchantListViewDetailState createState() => _MerchantListViewDetailState(this.merchant,this.merchantController);
}

class _MerchantListViewDetailState extends State<MerchantListViewDetail> {

  final Merchant merchant;
  final MerchantController merchantController;

  _MerchantListViewDetailState(this.merchant,this.merchantController);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: globals.backgroundColor,
      appBar: AppBar(
        title: Text(merchant.getName()),
        centerTitle: true,
      ),
      body: Container(
        child: SingleChildScrollView(
          child: MerchantDetailWidget(merchant: merchant,merchantController: merchantController),
        ),
      ),
    );
  }
}
