import 'package:get_it/get_it.dart';
import 'package:quandoo_home_task/helper/api_manager.dart';

/// This class is for injection dependency
///
/// For this i use "GET IT" we can register our
/// object to inject in different places
///

GetIt locator = GetIt.instance;


void setUpGetItLocator(){
  locator.registerFactory(() => ApiManager());

}