import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:quandoo_home_task/merchant/merchant_controller.dart';
import 'package:quandoo_home_task/merchant/merchant_item_detail_view.dart';
import 'package:quandoo_home_task/merchant/merchant_list_view.dart';
import 'package:quandoo_home_task/model/merchant.dart';

/// Class to manage navigations across the app from one screen to another.
///
/// This class include several methods which are redirecting users
/// to another screen.
class NavigationManager {
  /// Navigate to Merchant List View
  /// Parameters : [BuildContext]
  static void navigateToMerchantListView(BuildContext context) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => MerchantListView()),
        (route) => false);
  }

  /// Navigate to Merchant List Detail View
  ///   /// Parameters : [BuildContext], MerchantController , index
  static void navigateToMerchantListDetailView(BuildContext context,
      Merchant merchant, MerchantController merchantController) {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => MerchantListViewDetail(
                  merchant: merchant,
                  merchantController: merchantController,
                )));
  }
}
