import 'dart:ui';

/// we will keep our static String and static colors
/// like Base Url's and other static Strings



// Url for Merchant List
String urlForMerchant="https://api.quandoo.com/v1/merchants?";

// Connection failed Strings
String connectionAlertTitle= "Connection failed";
String connectionAlertMsg= "Check your internet connection";


// error message
String errorTitle="Error";
String errorMessage="There is some thing wrong with the application. Please, restart the application";





/// colors
const dividerColor = Color(0xff454545);

const listTextColor = Color(0xff1376ad);

const backgroundColor = Color(0xffffffff);

const blackColor = Color(0xff000000);

const lowReviewColor = Color(0xfff59542);

const highReviewColor = Color(0xff56db21);