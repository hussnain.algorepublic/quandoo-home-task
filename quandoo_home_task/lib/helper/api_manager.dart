

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:quandoo_home_task/helper/common.dart';
import 'package:quandoo_home_task/model/merchant.dart';
import 'package:quandoo_home_task/model/merchant_entity_model.dart';
import 'global.dart' as globals;
import 'package:http/http.dart' as http;

class ApiManager{
  MerchantEntityModel merchantEntityModel;
  List<Merchant> merchants=[];


  Future<bool> getMerchantList(BuildContext context,int limit) async {

   await Common.checkInternetConnection().then((success) async{
    if(success){
      String url="${globals.urlForMerchant}limit=$limit";
      var jsonResponse;
      print(url);
      var response = await http.get(Uri.parse(url));
      print(response.body);
      jsonResponse = json.decode(response.body);
      if (response.statusCode == 200) {
        merchantEntityModel = merchantEntityModelFromJson(json.encode(jsonResponse));
        merchants.addAll(merchantEntityModel.merchants);
        return true;
      } else {
        Common.showToastMessage(context,jsonResponse["errorMessage"] );
        return false;
      }
    }
    else{
      Common.showAlert(context, globals.connectionAlertTitle, globals.connectionAlertMsg);
      return false;
    }



   });

  }


}