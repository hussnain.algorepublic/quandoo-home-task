import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import "global.dart" as globals;

/// std font size
const double stdFontSize = 22;

/// text styles to all inputs and forms
class TextStyles {

  /// this style use for list text
  static TextStyle getListTextStyle(Color color) {
    return GoogleFonts.sairaSemiCondensed(
        color: color, fontWeight: FontWeight.w600, fontSize: 18);
  }

  /// this style use for list text
  static TextStyle getNameStyle(Color color) {
    return GoogleFonts.sairaSemiCondensed(
        color: color, fontWeight: FontWeight.w600, fontSize: stdFontSize);
  }


  /// this style use for list text
  static TextStyle getUnderLineTextStyle(Color color) {
    return GoogleFonts.sairaSemiCondensed(
        color: color, fontWeight: FontWeight.w600, fontSize: 18,
        decoration: TextDecoration.underline);
  }

  /// this style use for list text
  static TextStyle getReviewTextStyle(Color color) {
    return GoogleFonts.sairaSemiCondensed(
        color: color, fontWeight: FontWeight.w600, fontSize: stdFontSize,
        );
  }


}
