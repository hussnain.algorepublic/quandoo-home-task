
import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:toast/toast.dart';
import 'package:map_launcher/map_launcher.dart' as mapLauncher;
import 'package:url_launcher/url_launcher.dart';



/// In this class we will keep our common function
/// That will user in different places in code

class Common {


  Future<bool> testInternet() async{
    return await checkInternetConnection();
  }

  /// This function is used for check the internet connection
  static Future<bool> checkInternetConnection()async{
    var connectivityResult;
    try {
     connectivityResult = await (Connectivity().checkConnectivity());
    } on PlatformException catch (e) {
      print(e.toString());
      return false;
    }
      if (connectivityResult == ConnectivityResult.mobile) {
        return true;
      } else if (connectivityResult == ConnectivityResult.wifi) {
        return true;
      }
      return false;
  }

  /// This function make toast message
  /// Parameters : [BuildContext], String message
  static void  showToastMessage(BuildContext context,String message){
    Toast.show(message, context,duration: 2, gravity: Toast.BOTTOM);
  }


  /// This function is use to launch map
  /// Parameters : String address, double lat, long
  static Future<void> launchMap(String address, double lat, long) async {
    var query = Uri.encodeComponent(address);
    if (Platform.isAndroid) {
      if (await mapLauncher.MapLauncher.isMapAvailable(
          mapLauncher.MapType.google)) {
        await mapLauncher.MapLauncher.showMarker(
          mapType: mapLauncher.MapType.google,
          coords: mapLauncher.Coords(lat, long),
          title: address,
        );
      }
    } else if(Platform.isIOS) {
      if (await mapLauncher.MapLauncher.isMapAvailable(
          mapLauncher.MapType.apple)) {
        await mapLauncher.MapLauncher.showMarker(
          mapType: mapLauncher.MapType.apple,
          coords: mapLauncher.Coords(lat, long),
          title: address,
        );
      }
    }
  }

  /// This function is use to make a call
  /// Parameters : String url
  static Future<void> launchLink(String url) async {
    print(url);
    if (await canLaunch(url)) {
      await launch(url);
    } else {

    }
  }

  /// Show Alert Method to show Alter message
  /// Parameters : [BuildContext], String title, String message
  static void showAlert(BuildContext context, String title, String msg) {
    showDialog(
        context: context,
        builder: (context) => CupertinoAlertDialog(
          title: Text(title),
          content: Text(msg),
          actions: <Widget>[
            CupertinoDialogAction(
              child: Text('OK'),
              onPressed: () async {
                Navigator.of(context).pop();
              },
            ),
          ],
        ));
  }


}