import 'package:http/http.dart' show Client;
import 'package:quandoo_home_task/helper/global.dart' as globals ;
import 'package:http/http.dart' as http;

class ApiProviderForTesting {
  final Client client;

  ApiProviderForTesting(this.client);

  Future<bool> getMerchantList(int limit) async {

        String url="${globals.urlForMerchant}limit=$limit";
        print(url);
        var response = await http.get(Uri.parse(url));
        print(response.body);
        if (response.statusCode == 200) {
          return true;
        } else {
          return false;
        }
      }

}