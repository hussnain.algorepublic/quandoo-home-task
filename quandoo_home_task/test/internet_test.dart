import 'dart:io';

import 'package:flutter_test/flutter_test.dart';


Future<void> main() async {

  testWidgets(
    'connectivity test',
        (tester) async {
      await tester.runAsync(() async {
        bool isConnected=false;
        try {
          final result = await InternetAddress.lookup('www.google.com',type: InternetAddressType.any);
          if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
            isConnected=true;
          }
        } on SocketException catch (_) {
          isConnected=false;
        }
        expect(isConnected, true);
      });
    },
  );
}
