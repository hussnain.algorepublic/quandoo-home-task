import 'dart:io';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:http/testing.dart';
import 'api_provider_for_testing.dart';
void main() {
  test('Merchant api testing', () async {
    Future<http.Response> _mockRequest(http.Request request) async {
      if (request.url
          .toString()
          .startsWith('https://jsonplaceholder.typicode.com/posts/')) {
        return http.Response(
            File('test/test_resources/random_user.json').readAsStringSync(),
            200,
            headers: {
              HttpHeaders.contentTypeHeader: 'application/json',
            });
      }
      return http.Response('Error: Unknown endpoint', 404);
    }

    final apiProvider = ApiProviderForTesting(MockClient(_mockRequest));
    final isSucceed = await apiProvider.getMerchantList(2);
    expect(isSucceed, true);


  });
}