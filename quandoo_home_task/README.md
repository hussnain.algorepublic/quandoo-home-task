# quandoo_home_task 

There are 2 things in this Application 

1. Merchant List View
2. Selected Merchant Detail View

Merchant list I am getting it from the server by using HTTP client. In this application, I am using MVC architecture 

In the main list, if you make orientation landscape so you can have both views list and details at the same time 

In the detail screen, I have made some clickable fields e.g. phone number, website, address 

I have mostly commented on every function and classes 

#Test coverage 

I have written 2 unit test 

1. Internet connectivity test (internet_test.dart)
2. API test (api_provider_for_testing.dart) 

I hope you will find it a good code 
